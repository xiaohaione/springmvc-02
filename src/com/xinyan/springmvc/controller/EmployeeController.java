package com.xinyan.springmvc.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xinyan.springmvc.dao.DepartmentDao;
import com.xinyan.springmvc.dao.EmployeeDao;
import com.xinyan.springmvc.pojo.Department;
import com.xinyan.springmvc.pojo.Employee;

/**
 * RESTful URL 设计：
 * 员工列表：         /emps	GET
 * 添加员工页面：  /emp	    GET
 * 添加员工：        /emp	    POST
 * 删除员工：       /emp/1    DELETE
 * 修改员工页面：/emp/1    GET
 * 修改员工：      /emp      PUT
 *
 */
@Controller
public class EmployeeController {
	
	@Autowired
	private EmployeeDao employeeDao;
	
	@Autowired
	private DepartmentDao departmentDao;
	
	/**
	 * 修改员工
	 * @param employee
	 * @return
	 */
	@RequestMapping(value = "/emp", method = RequestMethod.PUT)
	public String updateEmployee(Employee employee) {
		//employee：是表单提交的对象
		//emp： 是数据库查询出来的对象
		Employee emp = employeeDao.get(employee.getId());
		
		emp.setEmail(employee.getEmail());
		emp.setGender(employee.getGender());
		Department dept = departmentDao.getDepartment(employee.getDepartment().getId());
		emp.setDepartment(dept);

		employeeDao.save(emp);
		return "redirect:/emps";
	}
	
	/**
	 * 修改员工页面
	 * @param id
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/emp/{id}", method = RequestMethod.GET)
	public String updateEmployeePage(@PathVariable("id") Integer id, Map<String, Object> map) {
		Employee employee = employeeDao.get(id);
		map.put("employee", employee);
		map.put("depts", departmentDao.getDepartments());
		return "edit";
	}
	
	/**
	 * 删除员工
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/emp/{id}", method = RequestMethod.DELETE)
	public String deleteEmplyoee(@PathVariable("id") Integer id) {
		employeeDao.delete(id);
		return "redirect:/emps";
	}
	
	/**
	 * 添加员工
	 * @param emp
	 * @return
	 */
	@RequestMapping(value = "/emp", method = RequestMethod.POST)
	public String addEmployee(Employee employee) {
		Department dept = departmentDao.getDepartment(employee.getDepartment().getId());
		employee.setDepartment(dept);
		employeeDao.save(employee);
		return "redirect:/emps";
	}
	
	/**
	 * 添加员工页面
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/emp", method = RequestMethod.GET)
	public String addEmployeePage(Map<String, Object> map) {
		map.put("depts", departmentDao.getDepartments());
		return "edit";
	}

	/**
	 * 员工列表
	 * @param map
	 * @return
	 */
	@RequestMapping("/emps")
	public String emps(Map<String, Object> map) {
		map.put("emps", employeeDao.getAll());
		return "list";
	}
}
