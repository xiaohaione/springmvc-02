<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath }/css/default.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="${pageContext.request.contextPath }/script/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
	$(function(){
		$(".del").click(function(){
			if(confirm("确认要删除？")){
				$("#subForm").attr("action", this.href).submit();
			}
			//阻止 a 的默认行为
			return false;
		})
	})
</script>
</head>
<body>
	<a href="${pageContext.request.contextPath }/emp">新增员工</a>
	<h3>员工列表</h3>
	<table class="table-basic">
		<tr>
			<th>ID</th>
			<th>LastName</th>
			<th>Email</th>
			<th>Gender</th>
			<th>Department</th>
			<th>Edit</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${requestScope.emps }" var="emp">
		<tr>
			<td>${emp.id }</td>
			<td>${emp.lastName }</td>
			<td>${emp.email }</td>
			<td>${emp.gender == 0 ? "Male" : "Female" }</td>
			<td>${emp.department.departmentName }</td>
			<td><a href="${pageContext.request.contextPath }/emp/${emp.id }">Update</a></td>
			<td><a class="del" href="${pageContext.request.contextPath }/emp/${emp.id }">Delete</a></td>
		</tr>
		</c:forEach>
	</table>
	
	<form id="subForm" action="" method="post">
		<input type="hidden" name="_method" value="DELETE">
	</form>
</body>
</html>