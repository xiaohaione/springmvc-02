<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath }/css/default.css" rel="stylesheet" type="text/css">
</head>
<body>
	<h3>Add / Update Employee</h3>
	<form action="${pageContext.request.contextPath }/emp" method="post">
		<c:if test="${employee != null }">
			<input type="hidden" name="id" value="${employee.id }">
			<input type="hidden" name="_method" value="PUT">
		</c:if>
		<table class="table-basic">
			<c:if test="${employee == null }">
				<tr>
					<th>LastName</th>
					<td><input type="text" name="lastName" value="${employee.lastName }" /></td>
				</tr>
			</c:if>
			<tr>
				<th>Email</th>
				<td><input type="text" name="email" value="${employee.email }" /></td>
			</tr>
			<tr>
				<th>Gender</th>
				<td>
					<input type="radio" name="gender" value="1" ${employee.gender == 1 ? "checked" : "" } />Female
					<input type="radio" name="gender" value="0" ${employee.gender == 0 ? "checked" : "" } />Male
				</td>
			</tr>
			<tr>
				<th>Department</th>
				<td>
					<select name="department.id">
						<c:forEach items="${requestScope.depts }" var="dept">
							<option value="${dept.id }" ${employee.department.id == dept.id ? "selected" : "" }>${dept.departmentName }</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th></th>
				<td><input type="submit" value="Submit" /></td>
			</tr>
		</table>
	</form>
</body>
</html>